import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


data = pd.DataFrame({
    "Имя": ["Данияр", "Дамир", "Фариза", "Жанибек", "Адилет", "Досымжан", "Меиржан","Еркеназ","Нурмахан","Аманжол", "Акнур"],
    "Пол": ["М", "М", "Ж", "М", "М", "М","М", "Ж", "М","М","Ж"],
    "Средний GPA": [3.72, 3.5, 2.8, 3.5, 3.5, 3.77, 2.0, 3.51, 3.0, 2.8, 3.5],
    "Имеет стипендию": [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    "Повышенная стипендия": [1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1]  # Целевая переменная (1 - получает, 0 - не получает)
})

X = data[["Пол", "Средний GPA", "Имеет стипендию"]]
y = data["Повышенная стипендия"]


le = LabelEncoder()
X["Пол"] = le.fit_transform(X["Пол"])

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)

decision_tree = DecisionTreeClassifier(random_state=42)
random_forest = RandomForestClassifier(random_state=42)
decision_tree.fit(X_train, y_train)
random_forest.fit(X_train, y_train)

dt_predictions = decision_tree.predict(X_test)
rf_predictions = random_forest.predict(X_test)


base_predictions = pd.DataFrame({
    "DecisionTree": dt_predictions,
    "RandomForest": rf_predictions
})


meta_model = LogisticRegression(random_state=42)
meta_model.fit(base_predictions, y_test)


selected_student = pd.DataFrame({
    "Пол": [1],  # 0 - Мужчина, 1 - Женщина
    "Средний GPA": [3],  # Средний GPA выбранного студента
    "Имеет стипендию": [1]  # 1 - Имеет обычную стипендию, 0 - Не имеет
})


selected_dt_prediction = decision_tree.predict(selected_student)
selected_rf_prediction = random_forest.predict(selected_student)


selected_base_predictions = pd.DataFrame({
    "DecisionTree": selected_dt_prediction,
    "RandomForest": selected_rf_prediction
})


stacking_result = meta_model.predict(selected_base_predictions)

if stacking_result[0] == 1:
    print("Выбранный студент получает повышенную стипендию.")
else:
    print("Выбранный студент не получает повышенную стипендию.")
